#[macro_use]
extern crate log;

struct Scalings {
    diff: f64,
    factor: f64,
    maxdiff: f64,
    prevdiff: f64,
    delta: f64,
    decrease: f64,
}

pub fn calccounts(data: &[f64], partition: f64) -> (usize, usize, f64, f64) {
    let mut below = -std::f64::INFINITY;
    let mut above = std::f64::INFINITY;
    let mut nlow = 0;
    let mut nhigh = 0;
    for &value in data.iter() {
        if value <= partition {
            nlow += 1;
        }
        if value >= partition {
            nhigh += 1;
        }
        if value < partition && below < value {
            below = value;
        }
        if value > partition && above > value {
            above = value;
        }
    }
    (nlow, nhigh, below, above)
}

fn calc_partition(
    scalings: &mut Scalings,
    partition: f64,
    prevpartition: &mut f64,
    value1: f64,
    value2: f64,
    min: f64,
    max: f64,
) -> f64 {
    if scalings.diff > scalings.maxdiff {
        if scalings.diff > scalings.prevdiff {
            // The change was overestimated
            // Try again with a smaller scaling factor
            // Change `factor` by a minimum of `decrease`, but
            // more if we overestimated the change a lot
            let ratio = scalings.prevdiff / scalings.diff;
            if ratio < scalings.decrease {
                scalings.factor *= ratio;
            } else {
                scalings.factor *= scalings.decrease;
            }
            debug!(
                "< factor, ratio, decrease = {}, {}, {}",
                scalings.factor, ratio, scalings.decrease
            );
            let sign = if value2 > value1 { 1.0 } else { -1.0 };
            let partition =
                *prevpartition + scalings.prevdiff * scalings.factor * scalings.delta * sign;

            if partition < min {
                (*prevpartition + min) / 2.0
            } else if partition > max {
                (*prevpartition + max) / 2.0
            } else {
                partition
            }
        } else {
            scalings.prevdiff = scalings.diff;
            *prevpartition = partition;
            scalings.delta = value1 - value2;
            let partition = partition + scalings.diff * scalings.factor * scalings.delta;
            if partition < min {
                (*prevpartition + min) / 2.0
            } else if partition > max {
                (*prevpartition + max) / 2.0
            } else {
                partition
            }
        }
    } else {
        value1
    }
}

fn calc_final_partition(
    nsame: usize,
    evenlen: bool,
    below: f64,
    above: f64,
    partition: f64,
) -> f64 {
    if nsame > 0 {
        if evenlen && nsame == 1 {
            (below + partition) / 2.0
        } else {
            partition
        }
    } else if evenlen {
        (below + above) / 2.0
    } else {
        below
    }
}

pub fn calculate(data: &[f64], maxdiff: f64, factor: f64, decrease: f64) -> f64 {
    let len = data.len();
    if len == 0 {
        return std::f64::NAN;
    }
    if len == 1 {
        return data[0];
    }
    if len == 2 {
        return (data[0] + data[1]) / 2.0;
    }

    let maxdiff = if maxdiff >= 0.0 {
        maxdiff // absolute value
    } else {
        -maxdiff * len as f64 // fraction of data size
    };

    let mut scalings = Scalings {
        diff: maxdiff,
        factor,
        maxdiff,
        prevdiff: std::f64::INFINITY,
        delta: 0.0,
        decrease,
    };

    let sum: f64 = data.iter().sum();

    let min = data.iter().fold(
        data[0],
        |min, value| if *value < min { *value } else { min },
    );
    let max = data.iter().fold(
        data[0],
        |max, value| if *value > max { *value } else { max },
    );
    let evenlen = len % 2 == 0;
    let mut partition = sum / (len as f64);
    let mut prevpartition = partition;

    debug!(
        "Partition start (= mean), maxdiff: {}, {}",
        partition, maxdiff
    );

    let mut iloop: u64 = 0;
    loop {
        iloop += 1;
        let (nlow, nhigh, below, above) = calccounts(data, partition);
        // Determine break criteria for the loop, or otherwise the
        // change in partition
        let nsame = nhigh + nlow - len;
        if nlow == nhigh {
            if nsame == 0 {
                partition = (below + above) / 2.0;
            }
            break;
        } else {
            let (delta, value1, value2) = if nlow > nhigh {
                (nlow - nhigh, below, above)
            } else {
                (nhigh - nlow, above, below)
            };
            if delta <= nsame {
                partition = calc_final_partition(nsame, evenlen, value1, value2, partition);
                break;
            }
            scalings.diff = (delta - nsame) as f64;
            debug!("diff, nsame = {}, {}", scalings.diff, nsame);
            partition = calc_partition(
                &mut scalings,
                partition,
                &mut prevpartition,
                value1,
                value2,
                min,
                max,
            );
        }
        debug!(
            "nlow, nhigh, below, above, delta = {}, {}, {}, {}, {}",
            nlow, nhigh, below, above, scalings.delta
        );
        debug!("iloop, partition: {}, {}", iloop, partition);
        debug!("");
    }
    debug!("iloop = {}", iloop);

    partition
}

pub fn calc(data: &[f64]) -> f64 {
    calculate(data, 5.0, 0.2, 0.5)
}

#[cfg(test)]
mod tests {
    use super::*;
    const EPS: f64 = 2.0 * std::f64::EPSILON;

    #[test]
    fn median0() {
        let data: Vec<f64> = vec![];
        let m = calc(&data);
        assert!(m.is_nan());
    }

    #[test]
    fn median1() {
        let data: Vec<f64> = vec![5.0];
        assert!((calc(&data) - 5.0).abs() <= EPS);

        let data = vec![std::f64::INFINITY];
        let m = calc(&data);
        assert_eq!(m, std::f64::INFINITY);
    }

    #[test]
    fn median2() {
        let data: Vec<f64> = vec![5.0, 6.0];
        assert!((calc(&data) - 5.5).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, 5.0];
        assert!((calc(&data) - 5.0).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, std::f64::INFINITY];
        let m = calc(&data);
        assert_eq!(m, std::f64::INFINITY);
    }

    #[test]
    fn median3() {
        let data: Vec<f64> = vec![5.0, 6.0, 7.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![7.0, 6.0, 5.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, 7.0, 6.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![6.0, 7.0, 5.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![6.0, 5.0, 7.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![7.0, 5.0, 6.0];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, 5.0, 7.0];
        assert!((calc(&data) - 5.0).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, 5.0, 5.0];
        assert!((calc(&data) - 5.0).abs() <= EPS);

        let data: Vec<f64> = vec![5.0, 6.0, std::f64::INFINITY];
        assert!((calc(&data) - 6.0).abs() <= EPS);

        let data: Vec<f64> = vec![std::f64::INFINITY, 6.0, std::f64::INFINITY];
        assert_eq!(calc(&data), std::f64::INFINITY);
    }

    #[test]
    fn median() {
        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 1.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 1.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 2.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 2.0).abs() <= EPS);
        let data: Vec<f64> = vec![4.0, 2.0, 1.0, 7.0, 3.0, 6.0, 5.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![7.0, 7.0, 1.0, 1.0, 5.0, 4.0, 3.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![5.0, 3.0, 4.0, 7.0, 1.0, 6.0, 2.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![3.0, 5.0, 4.0, 8.0, 1.0, 7.0, 2.0, 6.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![4.0, 6.0, 3.0, 8.0, 1.0, 7.0, 2.0, 5.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![5.0, 6.0, 3.0, 8.0, 1.0, 7.0, 2.0, 4.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 4.0];
        assert!((calc(&data) - 3.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 4.0];
        assert!((calc(&data) - 3.0).abs() <= EPS);
    }

    #[test]
    fn moremedian() {
        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 4.0];
        assert!((calc(&data) - 3.0).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![3.0, 5.0, 4.0, 8.0, 1.0, 7.0, 2.0, 6.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 4.0];
        assert!((calc(&data) - 3.0).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 1.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 1.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 2.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 2.0).abs() <= EPS);
        let data: Vec<f64> = vec![4.0, 2.0, 1.0, 7.0, 3.0, 6.0, 5.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![7.0, 7.0, 1.0, 1.0, 5.0, 4.0, 3.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![5.0, 3.0, 4.0, 7.0, 1.0, 6.0, 2.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 1.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 1.0).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 1.0, 2.0, 4.0, 5.0, 6.0, 1.0];
        assert!((calc(&data) - 2.0).abs() <= EPS);
        let data: Vec<f64> = vec![4.0, 2.0, 1.0, 7.0, 3.0, 6.0, 5.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![7.0, 7.0, 1.0, 1.0, 5.0, 4.0, 3.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);
        let data: Vec<f64> = vec![5.0, 3.0, 4.0, 7.0, 1.0, 6.0, 2.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 4.0, 5.0, 6.0, 7.0];
        assert!((calc(&data) - 4.0).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 6.0, 20.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);
        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0, 5.0, 6.0, 6.0];
        assert!((calc(&data) - 4.5).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 3.0, 5.0];
        assert!((calc(&data) - 2.5).abs() <= EPS);

        let data: Vec<f64> = vec![1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0];
        assert!((calc(&data) - 16.0).abs() <= EPS);
    }
}
